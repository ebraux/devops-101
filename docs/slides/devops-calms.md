---
marp: true
paginate: true
theme: default
---
<!--  #0074d0 
 le marron-->
<!-- #ff5001 le orange -->
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:40% 100%](assets/devops.png)



# Devops - CALMS

## Les piliers du devops

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# CALMS - Les piliers du devops

### Culture
### Automatisation
### Lean
### Mesures
### Sharing

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](assets/cc-by-nc-sa.png)

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

---
![bg left:30% 60%](img/keep-CALMS.png) 
Les piliers du Devops. 

1. **Culture** : accompagnement, conduite du changement, implication
2. **Automatisation** : déploiement continu et Infra as code
3. **Lean** : production de valeur, minimisation des coût et "déchets"
4. **Mesures** : nécessaire pour s'améliorer
5. **Sharing** : partages des informations, collaboration

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## Culture

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Culture 

**Le DevOps est avant tout une histoire de culture et de collaboration.**

- DevOps est plus qu’un simple outil ou un changement de processus.
- Les outils technique ne sont pas suffisants.
- La culture compte pour plus de la moitié du DevOps.

**Le devops exige le changement de la culture organisationnelle**

Inspiré de l'agilité : "Les individus et les interactions plutôt que les processus et les outils"

---
# Les relations entre les équipes

**Il faut créer la volonté de travailler ensemble - et apprendre à le faire**

- Des problèmes humains, des problèmes de communication, ...
- Un grand nombre d'équipes : les opérations, les testeurs, les designers, les développeurs, les chefs de projet, ...
- Accompagner le changement de culture et de fonctionnement
  
---
# Création d'équipes orientées "produit" 

- Identifier les compétences nécessaires
- Casser les silos
- Points réguliers : daily meeting [agile]
  - intégrer tous les acteurs
  - échanger sur les problématiques (opérations, aux développements, ...)
  - chaque équipe doit voir la valeur que l’autre apporte
  - pollinisation croisée des compétences.

---
# Accepter l'Erreur

- L'expérimentation : 
  - il faut essayer, expérimenter et gagner en connaissance et en maturité
  - ce n'est pas grave de se tromper. 


---
# Les questions à se poser

- Est-ce que les problèmes sont résolus par **toute l'équipe produit** ?
- Est-ce que **tous** les membres de l'équipe partagent une **vision commune** ?
- Est-ce que les analyses après problèmes sont dirigés vers la **résolution des problèmes** et non vers la détermination d'un responsable de l'erreur ?
  
---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## Automatisation

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Automatisation

**"Le déploiement d'une nouvelle version ne doit plus être un événement"**

- Déployer plus souvent
- Mieux contrôler les déploiements
- S'assurer que les corrections sont bien prises en compte
  
---
# Historiquement : des déploiements Complexes

- Mode de déploiement : rédaction d'un manuel
- Risque d'erreur : redaction , lecture, implémentation
- Ajustement lors du déploiement en prod : information par mail, orale, ... information perdue
- Test d'impact d'une modification ?

**Les déploiements consomment beaucoup de ressources, sont risqués, et donc sont rares.**

---
# Déploiements automatisés

## Objectif : aucune intervention manuelle pendant le déploiement

- Tester/répéter les déploiements
- Améliorer : à chaque problème 
  - corriger le processus de déploiement
  - prendre en compte le problème dans les tests
- Prendre confiance petit à petit

---
# Automatiser c'est :

- pouvoir dupliquer les environnements : éviter les conflits (ça marche chez moi!)
- réduire les risques d'erreurs
- maîtriser les évolutions/changements
- documenter automatiquement

---
# Créer des environnements à la demande

- outils d'Infrastructure-as-Code : environnements
  - versionables : historisation, retour arrière
  - duplicables : dev, tests, recette, prod...
- plus besoin d'attendre plusieurs jours (mois) la mise à disposition d'un environnement
- disponibilité quasi immédiate : quelques minutes.
- économique : on détruit l'environnement après l'avoir utilisé
 
---
# Garantie de bon fonctionnement

- le code de l'application est versionné
- l'environnement d'infrastructure est versionné

**Le déploiement de l'application et de son environnement d'execution est reproductible :**
- rend possible plus de tests sur le code et sur l'environnement
- permet de granulariser les développements 
- permet de répéter, s'entraîner au passage en production
- permet des passages en production progressifs et réversibles

---
# Le déploiement, un non événement

- L'automatisation fait généralement passer de 3 à 4 déploiements annuels, à des déploiements hebdomadaires, voir quotidiens.
- Des entreprises comme Google, Amazon et Facebook, peuvent déployer jusqu'à 20 fois par minute en production.


---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## Lean

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Lean

Lean, ou Lean Management est une méthode de gestion de production
- centrée sur la rationalisation des opérations,
  -  en réduisant les déchets
  -  et maximisant la valeur du client.
- vient de l'industrie, et plus particulièrement de Toyota dans les années 1990.

**Source d’inspiration pour la culture DevOps.**

---
![bg left:40% 90%](img/7-wastes-lean.png) 
# 7 sources de gaspillage

- Stocks
- Attente
- Les défauts/Corrections/retouches
- Surproduction
- Mouvements/Étapes inutiles
- Transports
- La sous-utilisation des compétences

*[https://blog.operaepartners.fr/2017/06/09/les-gestes-inutiles-dans-un-back-office/](https://blog.operaepartners.fr/2017/06/09/les-gestes-inutiles-dans-un-back-office/)*

---
# Lean et Devops

Délivrer de la valeur ajoutée au client final, en minimisant  les processus :
- longs,     
- coûteux
- sans valeur ajoutée

Amélioration continue :
- des petites améliorations faites au quotidien, constamment.
- accepter les erreurs, les problèmes, et s'y préparer
- Approche "Kaizen" : gestion de la qualité impliquant tous les acteurs
  
*[https://fr.wikipedia.org/wiki/Kaizen](https://fr.wikipedia.org/wiki/Kaizen)*

---
# En Pratique

Réduire la taille des déploiements :
- éviter de tout "rebuilder" à la moindre modification
- limiter la durée des tests

Mettre en place une chaîne de valeur "Value Stream map" (VSM)
- Identifier les processus qui prennent du temps, sans valeur ajoutée
- ex : déploiement d'un environnement.
  - Prend plusieurs semaines
  - achat de matériel 
  - demandes d'autorisations
  - disponibilité des personnes pouvant intervenir sur la configuration

--- 
# En Pratique

En cas de problème ou d'incident
- ne pas le dissimuler ou le minimiser
- ne pas rechercher des responsables, ni les stigmatiser
- encourager les échanges entre les différents acteurs sur ce sujet
- faire un bilan, une rétrospective : identifier les problèmes
- mettre en place des processus pour les corriger, et les détecter

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## Measurement

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Measurement (Mesures)

Le Devops transforme l'entreprise, mais demande des efforts

- Quel est son impact ?
- Est-il positif ou négatif ?
- Quelle est la dynamique ?
   
**Il est nécessaire d'avoir des indicateurs de performance clés (KPI ou Key Performance Indicator)**

--- 
# Les mesures

A tous les niveaux :
- au niveau système : charge CPU, mémoire, ...
- au niveau applicatif : temps d'execution des fonctions, empreinte mémoire, ...
- au niveau outillage : nombre de bugs, suivi des demandes de support
- expérience utilisateur : traçage de l'utilisation de fonctionnalité, nombre de clics, ...
- ...

--- 
# Exemples d'indicateurs

- Durée du cycle "développement às production" pour une nouvelle fonctionnalité
- Nombre d'apparition d'un bug récurrent
- Nombre de personnes utilisant le produit en temps réel
- Nombre d'utilisateurs gagnés ou perdus en une semaine
- Latence d'une l'application 
- Rentabilité d'une nouvelle fonctionnalité : rapport investissement/utilisation
- ...
  

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## Sharing

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Sharing

- Partager / faire circuler l'information
- Ne rien cacher
- Produire des documents compréhensibles par tous
- Favoriser la collaboration au sein du groupe produit
- Partager les responsabilités

---
## Impliquer

- les DEV doivent connaître et comprendre les opérations réalisées par les OPS
  - mise en production
  - exploitation
  - ...
- les OPS doivent connaître et comprendre les opérations réalisées par les DEV 
  - livraisons
  - hotfix
  - ...

---
## Partager les responsabilités

- "ça c'est pas mon code" **-->** "c'est notre produit"
- "là, c'est plus ma partie" **-->** "qui va pouvoir aider pour aller plus loin"
- "c'est l'infra qui est mal paramétrée" **-->** "Il faut faire un focus pour améliorer le paramétrage"
- "ça a mal été testé" **-->** "Comment améliorer les tests pour éviter que ça ne se reproduise"
- "Ouf, c'est bon, mon appli est passé en prod" **-->** "Bravo tout le monde, tout c'est bien passé !!!"
- ...

---
## Être disponible

Permettre à chacun de se rendre disponible pour échanger sur une problématique

- "là je suis sur un truc urgent"
- "on verra ça plus tard"
- "on en discute quand tu peux"
- ...

Il faut l'organiser !!

---
# Les questions à se poser

- Les objectifs sont-ils clairs, et partagés
- Où sont les informations, la documentation  ?
- Comment la documentation est-elle produite, alimentée ? Par qui ?
- Est-ce que les DEV peuvent décrire le travail des OPS. Et réciproquement ?
- Est-ce que tout le monde a accès à tout ?
- Comment communique t'on ? (tickets, mails, messagerie instantanée, ... )
- Comment s'organisent les taches impliquant des DEV et des OPS
- Comment sont traités les échecs ?
- Comment sont fêtées réussite ?
- ...

------

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Les Piliers du devops : CALMS

## En conclusion

---
![bg left:30% 60%](img/keep-CALMS.png) 
# CALMS

1. **Culture** : accompagnement, conduite du changement, implication
2. **Automatisation** : déploiement continu et Infra as code
3. **Lean** : production de valeur, minimisation des coût et "déchets"
4. **Mesures** : nécessaire pour s'améliorer
5. **Sharing** : partages des informations, collaboration

