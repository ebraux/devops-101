---
marp: true
paginate: true
theme: default
---
<!--  #0074d0  le marron-->
<!-- #ff5001 le orange -->
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:40% 100%](assets/devops.png)

# Devops 101

## Introduction to Devops
### Basic Principles

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)


# Introduction to DevOps

- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**
  
---

# Licence informations


Author: -emmanuel.braux@imt-atlantique.fr-

This presentation is under Creative Commons 3.0 France License (CC BY-NC-SA 3.0 FR)
Under the options: Attribution - Non-Commercial Use - Share Alike.

![cc-by-nc-sa](assets/cc-by-nc-sa.png)


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- ## > Definition
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**

---


# Definition

"DevOps is a **methodology** in software engineering and a **technical practice** aimed at unifying software development (**DEV**) and IT infrastructure management (**OPS**), particularly system administration"
(Wikipedia)
  
*[https://en.wikipedia.org/wiki/DevOps](https://en.wikipedia.org/wiki/DevOps)*

---
### A methodology that drives management of software development and delivery :
  - Continuous development
  - Apply changes quickly into production (issues, requirements)
  - Automation to improve efficiency

### DevOps combine in one integrated workflow :
  - People, processes, and technologies
  - Development, testing, and operations of software

### Uses agile principles, best practices, and tools for optimal software development

---
# DevOps life cycle

![bg left:40% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/ci-cd.png)

- Iterating in sprints
- Starts with **planning** 
- Next, requirements are converted to **code** **built** and **tested**.
- Then **release** is generated automatically
- Follow by **deployment** to production.
- The software is **operated** and **monitored**.
- Feed back are used as new requirements
- The cycle then iterates


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- ## > Context
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**

--- 
# The DEVs

- Anyone involved in **the creation of software before it reaches production**
- Developers, product managers, testers, Product Owners, etc.
- Responsible for **producing innovation** and delivering **new features** to users as soon as possible
- Looking for **changes**

--- 
# The OPS

- Anyone involved in **the operation and maintenance of the production environment**
- System Engineers, Database Administrator, Network Engineers, etc.
- Responsible for ensuring that users have access to a **stable, fast, and responsive system**
- Looking for **organizational stability**


---
## Software Deployment Process: DEV Side

- Definition of product features
- Definition of prerequisites
- Planning
- Development
- Building packages
- Delivering a version of the application for production deployment

---
## Software Deployment Process: OPS Side

- Deployment of infrastructures
- Installation of prerequisites
- Deployment of the application
- Monitoring the application (incidents, etc.)
- Performance monitoring

---
# Organization: Waterfall Model

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cascade_avec_livrables.png)

---
![bg left:30% % opacity:.6](img/cascade_avec_livrables.png)
# Organization: Waterfall Model

- Linear phases
- Task specialization
- Each phase depends on the results of the previous phase
- In case of defect: return to the previous phase
- Very strong impact of requirements

*[https://en.wikipedia.org/wiki/Waterfall_model](https://en.wikipedia.org/wiki/Waterfall_model)*

---
# Organization: V-Model

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cycle_en_v.png)


---
![bg left:30% % opacity:.6](img/cycle_en_v.png)
# Organisation: Cycle en V
- Modèle cascade, mais en renforçant la validation
- Flux descendant,
  - détailler le produit jusqu'à sa réalisation
- Flux ascendant,
   -  valider le produit jusqu'à sa "recette" au client
- Découpage système / composants
- Impact toujours très fort des exigences
- "temps long" : évolution demande client ?

*[https://fr.wikipedia.org/wiki/Cycle_en_V](https://en.wikipedia.org/wiki/V-model)*

---
# Organization: Agile Method - Scrum

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/vueglobalescrum.png)

---
![bg left:30% % opacity:.6](img/vueglobalescrum.png)
# Organization: Agile Method - Scrum

- Adaptive planning, iterations
- Evolutionary development
- Early delivery
- Continuous improvement
- Flexible to change
- The Scrum method implements the agile approach

*[https://en.wikipedia.org/wiki/Agile_software_development](https://en.wikipedia.org/wiki/Agile_software_development)*
*[https://en.wikipedia.org/wiki/Scrum_(software_development)](https://en.wikipedia.org/wiki/Scrum_(software_development))*

---
![bg left:25% 90% ](img/ITIL-Cycle-de-vie-des-services.png)
# Organization: ITIL

**I**nformation **T**echnology **I**nfrastructure **L**ibrary

A set of best practices for IT management

A methodological framework:

- How to organize an information system?
- How to improve the efficiency of the information system?
- How to reduce risks?
- How to increase the quality of IT services?

*[https://en.wikipedia.org/wiki/ITIL](https://en.wikipedia.org/wiki/ITIL)*

---
![bg left:50% 90% ](img/ITIL.jpg)

# Organization: ITIL

- Service catalog,
- Planning,
- Strategy,
- Governance,
- Continuous improvement
- Risk management,
- ...

---
# DEV and OPS: Different Visions

|               DEV                |                 OPS                 |
| :------------------------------: | :---------------------------------: |
|    Planning and delivery date    | Quality of service and availability |
|       Latest technologies        |        Standard technologies        |
| Frequent and significant changes |    Minimize changes - Incidents     |
|          Agile Methods           |           V-Model - ITIL            |
|          Expect the OPS          |           Endure the DEVS           |

--- 

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/WallOfConfusion-1-768x341.png)

---

![bg left:60% 60% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/Code-bombing.png)
# And Very Often

## Hop ...
  

---

# DEV and OPS: Different Motivations

Criteria considered for bonuses, promotions, ...

- DEV:
  - number of bugs fixed
  - number of new features delivered
- OPS:
  - overall application availability throughout the year

---
# Are DEV and OPS the only ones concerned?

- Importance of **"Time to Market"**: time between the decision to create a feature and its availability on the final product in production.
- Ensuring a good customer experience: reliability, stability, performance
- Cost
- ...

Marketing, finance, human resources, ...


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- **Context**
- ## > Origin and Concepts
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**


--- 
![bg left:25% %](img/patrick_debois.png)
# How to Proceed

A question posed by Patrick Debois in 2007 ...
- Held several positions in large companies for 15 years: developer, network engineer, system administrator, and project manager
- Good technical and organizational experience
- Solution tester on a data center migration project
- Must collaborate with DEVs and OPS, reconcile the usages, work efficiently
- And it's complicated ...
  
*[https://www.linkedin.com/in/patrickdebois/](https://www.linkedin.com/in/patrickdebois/)*
*[https://www.jedi.be/](https://www.jedi.be/)*

---
# Finding Solutions ...

- 2008: "Agile Infrastructure" presentation by Andrew Shafer (RedHat)
  - Making OPS work in an agile way.
  - Few participants (including Patrick Debois)
  - Creation of a Google group: little enthusiasm.
- 2009: Velocity O'Reilly, Dev and Ops cooperation at Flickr, John Allspaw and Paul Hammond
  - Collaboration, positive experience, ...
  
*[https://groups.google.com/g/agile-system-administration](https://groups.google.com/g/agile-system-administration)*
*[https://www.slideshare.net/jallspaw/10-deploys-per-day-dev-and-ops-cooperation-at-flickr](https://www.slideshare.net/jallspaw/10-deploys-per-day-dev-and-ops-cooperation-at-flickr)*
*[https://www.youtube.com/watch?v=LdOe18KhtT4](https://www.youtube.com/watch?v=LdOe18KhtT4)*

---
# Birth of DevOps
  
- October 2009: the "Devopsdays"
  - Patrick Debois could not attend the presentation "Dev and Ops cooperation at Flickr"
  - He then created the "DevopsDays" in Gand, Belgium
  - Huge success: devs, ops, managers, leaders

- 2010: Birth of the Twitter("X") hashtag **#DevOps**

  
*[https://devopsdays.org/](https://devopsdays.org/)*

---
# Tendance depuis 2009

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-historique.png)

*[https://trends.google.fr/](https://trends.google.fr/)*

---
![bg 80% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-monde.png)
![bg 20% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-france.png)


---
![bg left:25% 95%](img/DevOpsDays.png) 
# Objectives of DevOps

- Satisfy the end user of the developed solution.
- Accelerate application deployments.
- Improve quality.
- Reduce "Time-to-Market".

---
![bg left:25% 95%](img/DevOpsDays.png) 
# Principles of DevOps

- A set of best practices
- Based on empiricism, the acquisition of knowledge through experience
- Involve all teams (not just Dev and Ops) in the entire service lifecycle
- Create a common culture

---
![bg left:25% 95%](img/DevOpsDays.png) 
# Improving the Efficiency of an IT Project

- By offering **continuous deployments**,
- While maintaining **environment stability**
- Through **monitoring** or **application security**.
- And using **automation** (testing, deployment)

---
![bg left:25% 95%](img/DevOpsDays.png) 
# DevOps

- Created by IT professionals for IT users
- Does not belong to any organization, vendors, or analysts, ...
- Not a product, not a specification, not a standard
- Not a title: a "DevOps engineer" does not exist


---  
![bg left:40% 100%](img/calms-model-devops.png) 

# The Pillars of DevOps

**Culture**: collaboration and communication
**Automation**: Continuous deployment and Infrastructure as Code.
**Lean**: value production, cost and "waste" minimization
**Measurement**: continuously collect data to drive improvements.
**Sharing**: knowledge, tools, and best practices 

---  
![bg left:45% 95%](img/DevOps-schema.png) 
# The Phases of DevOps

- **Plan**: Planning
- **Code**: Development
- **Build**: Building
- **Test**: Testing
- **Release**: Releasing
- **Deploy**: Deployment
- **Operate**: Operation
- **Monitor**: Monitoring

---  
![bg left:40% 90%](img/devops-methods.png) 
# DevOps Methods

- "CI/CD": Continuous Integration, Continuous Delivery/Deployment
- "Infrastructure As Code"
- "Shift Left Testing"
- ... 

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- **Context**
- **Origin and Concepts**
- ## > The Pillars of DevOps: CALMS
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**


---
![bg left:25% 60%](img/keep-CALMS.png) 
# CALMS - The pillars of DevOps.

1. **Culture**: Teams must feel fully involved in projects 
   - collaboration and communication
2. **Automation**: Automating repetitive tasks to increase efficiency and reduce human error
   - Continuous deployment and Infrastructure as Code.
3. **Lean**: Optimize processes, eliminate waste, reduce cost 
   - strategy focused on high-value-added
4. **Measurement**: continuously collect data to drive improvements.
5. **Sharing**: knowledge, tools, and best practices across teams to enhance collaboration and innovation.

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Culture

**DevOps is primarily about culture and collaboration.**

- DevOps requires a change in organizational culture
- There must be a willingness to work together
-   and learn how to do it...
- Creation of product-oriented teams
- Accepting mistakes

**Inspired by agility: "Individuals and interactions over processes and tools"**

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Automation

**"Deploying a new version should no longer be an event"**

- Deploy more often
- Better control, test/repeat deployments
- Infrastructure-as-Code tools: versionable and duplicable environments
- The deployment of the application and its execution environment must be reproducible

**Deployment = a non-event**


---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Lean

**Lean, or Lean Management, is a production management method focused on streamlining operations**

**Deliver added value to the end customer by minimizing long, costly, non-value-added processes.**

- Reduce the size of deployments
- Implement a "Value Stream Map" (VSM)
- In case of a problem or incident, conduct a review, a retrospective
- Continuous improvement
- ...


---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Measurement

DevOps transforms the company, but requires effort

- What is its impact?
- Is it positive or negative?
- What is the dynamic?

**It is necessary to have key performance indicators (KPIs)**

- Number of occurrences of a recurring bug
- Performance of the different components
- Number of users gained or lost in a week
- ...
 

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Sharing

- # CALMS - Sharing

- Circulate, relaying informations
- Involve, share responsibilities
- Organize transfer of knowledge, and training
- Produce documents understandable by everyone
- Promote collaboration within the product group



---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to Devops

- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- ## > The Phases of DevOps
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- **Technical Tools**


---

![bg left:40% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4](img/DevOps-schema.png ) 
# The Phases of DevOps

- Plan: Planning
- Code: Development
- Build: Building
- Test: Testing
- Release: Releasing
- Deploy: Deployment
- Operate: Operation
- Monitor: Monitoring

*[https://en.wikipedia.org/wiki/DevOps_toolchain](https://en.wikipedia.org/wiki/DevOps_toolchain)*

---
![bg left:25% % opacity:.7](img/plan.png)
# Plan: Planning

**Imagine and describe the features of applications and systems**

And define:
- Requirements
- Business value
- Indicators, Metrics, and production feedback
- Timing and cost-benefit analysis of releases
- Security policy
- ...
  
---
![bg left:25% % opacity:.7](img/code.png)
# Code: Development

**Writing code and configuring the software development process.**

All aspects of coding:
- Writing,
- Testing, quality and performance checks
- Reviews
  
---
![bg left:25% % opacity:.7](img/build.png)
# Build: Building

**Assembling the different code elements to "build" the application**

- Concerns the code produced by different developers
- Integration of code by team members
- Preparation for delivery

---
![bg left:25% % opacity:.7](img/test.png)
# Test: Testing

**Testing not at the code level, but at the application level**

- Acceptance testing,
- Regression testing,
- Security and vulnerability analysis,
- Performance and load testing,
- Resilience testing

---
![bg left:25% % opacity:.7](img/release.png)
# Release: Versioning

**Delivery of an application version**

- Tracking, Coordination of deliveries
- Planning, traceability
- Archiving necessary elements (Artifacts)

Maintaining history in case a rollback is needed

---
![bg left:25% % opacity:.7](img/deploy.png)
# Deploy: Deployment

**Deploy applications in production environments consistently and reliably**

- Includes deployment and configuration of the underlying infrastructure
- Allows for manual approvals on certain parts


---
![bg left:25% % opacity:.7](img/operate.png)
# Operate: Operationalization

**Monitoring the application within the infrastructure**

- Monitoring, orchestration
- Absorbing load peaks
- Adjusting the infrastructure architecture
  - Modifying the number of resources (scale up / down)
  - Partitioning, setting up Load-Balancers, ...

---
![bg left:25% % opacity:.7](img/monitor.png)
# Monitor: Monitoring

Involves the maintenance, supervision, and troubleshooting of applications in production environments.

- Infrastructure performance
- User experience and feedback
- Production metrics and statistics
- Complements "operate" but with the goal of feeding back into other phases for improvement.

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- ## > DevOps Methods
- **Implementation Challenges**
- **Roles**
- **Technical Tools**


---
# CI/CD - Continuous Integration

**Make recent code available to all developers**

- Enable short cycles,
- Build, test, and release software faster.

Phases:
- "Commit" the code to the repository, and notify
- Start the integration of the code, to verify it
  - Build test of the code: code quality, ...
  - Test the code: integration and acceptance

---
# CI/CD - Code Quality

**Objective: unify the code among all developers**

Define the coding standard to be used.
  - Coding style, rules to follow, etc.

Adhere to common standards:
  - Example standards: PEP8, FLAKE8, ...

---
# CI/CD - Continuous Delivery / Deployment

**Deploy changes systematically and controlled**

- Frequent and automated deliveries
- Reduces costs, time, and risks inherent to changes.
- Always a version available to be used by customers (releasable)
- Ability to roll back if needed

- _**Delivery**_: deployment remains manual.
- _**Deployment**_: the application is deployed automatically.

---
# Version Control

**Collaboration, task distribution, parallel development, history tracking**

- Track revisions and modification history
- Facilitate code review, rollback to a previous version
- Merge code changes, manage conflicts
- Most widely used version control system: Git

*[https://en.wikipedia.org/wiki/Git](https://en.wikipedia.org/wiki/Git)*

---
# Infrastructure as Code

**Design infrastructure and systems descriptively**

- Text files (usually YAML format)
- Deployment and monitoring tools
- Managed like code: version control, tests, etc.
- Reliable, reproducible, and controlled deployments
- Reduced risk of human error
- Environment duplication
- Most common Infrastructure as Code systems: Ansible, Terraform.

---
# Shift Left Testing

**The later a bug or defect is detected in the production chain, the greater its impact**

Integrate automated tests as early as possible.

![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/Shift-Left-Testing.png)

---
![bg left:30% 80%](img/3-ways.png)
# Three ways

From the book by Gene Kim, "The DevOps Handbook" and "The Phoenix Project":

- "First Way": notion of **flow**
- "Second Way": importance of **feedback**
- "Third Way": experiment and **continue** learning

--- 
## "First Way": notion of flow
- Importance of the performance of the entire system, not just its individual parts.
- Prohibit local optimizations
- Identify the most important limiting factor (the constraint)

## "Second Way": importance of feedback
- Reduce feedback loop delays
- Ensure customer feedback is taken into account as quickly as possible
- Peer review: have the code reviewed

---
## "Third Way": experiment and continue learning
- Create a culture of continuous experimentation
- Allocate time to improve daily work
- Create rituals
- Plan time for discovery (Testing, Hackathons, ...)

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- ## > Definition
- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- ## > Implementation Challenges
- **Roles**
- **Technical Tools**

---
# Culture

**Manage Change Effectively**
- Implement change management practices to handle changes smoothly and minimize disruptions.

> Cultural Resistance: Resistance to change within the organization can hinder or even prevent the adoption of DevOps practices.

---
# Collaboration

**Foster a Collaborative Culture**:
- Encourage open communication and cooperation between development, operations, and other stakeholders.

> Poor Collaboration: Lack of communication and cooperation can undermine DevOps efforts.

---
# Skills

**Encourage Continuous Learning and Improvement**
- Promote a culture of continuous learning and improvement through regular training, feedback, and experimentation.

> Lack of Skilled Personnel: Insufficient expertise in DevOps tools and practices can lead to ineffective implementation.

---
# Automation

**Automate Everything**
- Automate repetitive tasks such as testing, deployment, and infrastructure provisioning to increase efficiency and reduce errors.

**Adopt Infrastructure as Code (IaC)**:
- Manage and provision infrastructure as code to ensure consistency and reproducibility.

> Inadequate Automation: Lack of automation of key processes can result in inefficiencies and errors.

---
# Continuous Integration/Continuous Deployment (CI/CD)

**Implement Continuous Integration/Continuous Deployment (CI/CD)**:
- Integrate code changes frequently and deploy them automatically to production environments.

> Overly Ambitious Sprints: Large/complex changes negatively impact deployment frequency.

> Unrealistic Expectations: Expecting immediate results without understanding that DevOps is a continuous improvement process can lead to disappointment.

---
# Monitoring

**Continuous Monitoring and Logging**:
- Continuously monitor applications and infrastructure to detect and resolve issues promptly.

**Measure and Analyze Performance**:
- Use metrics and analytics to measure performance and identify areas for improvement.

> Insufficient Monitoring and Logging: Without proper monitoring and logging, it is difficult to maintain the health and performance of applications.

---
# Security

**Integrate Security Practices (DevSecOps)**:
- Embed security practices throughout the development lifecycle to ensure secure software delivery.

> Security Impact: Overly complex or restrictive security practices are often bypassed.

> Neglecting Security: Neglecting security practices can lead to vulnerabilities.

---
# Tools

**Use the Right Tools**:
- Select and integrate tools that are suitable for workflows, simple, and effective.

> Tool Overload: Using too many tools without proper integration can create complexity and confusion.


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- ## > Roles
- **Technical Tools**


---
# SRE (Site Reliability Engineering)

**Objective: create scalable and reliable systems**

- Originated at Google.
- Assign operational tasks to software engineers.

SRE Engineer:
- 50%: operational tasks (incidents, manual interventions, ...)
- 50%: development tasks (new features, task automation, ...)

---
# DevSecOps

**Create a global mindset where everyone feels responsible for the organization's security**

- Developers and operations
- Managers
- Administrative staff (secretaries, ...)

**Finding a balance to be responsive without hindering work.**

- Security as code, security by design
- Integrate into "shift left"

---
# ChatOps

**Objective: Optimize communication flows**

- Connect people, tools, files, ...
- Broadcast automatic messages:
  - when a development or deployment is done,
  - when support is needed
- Reduce MTTR (Mean Time To Repair)
  - Shorter feedback loop
  - Direct interaction between customers and developers

Use of BOTs in tools like Slack, Teams, Mattermost, ...

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction to DevOps

- **Definition**
- **Context**
- **Origin and Concepts**
- **The Pillars of DevOps: CALMS**
- **The Phases of DevOps**
- **DevOps Methods**
- **Implementation Challenges**
- **Roles**
- ## > Technical Tools


---

# Technical Tools

- No "DevOps" tools,
- Tools "Supporting the DevOps approach".
- Beware of "rebranded" automation or deployment tools
- A lot of tools
- Some already used in the company

---
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/gittoolsReport2020.png)

*[https://extasius.com/devops-tools-report-2020/](https://extasius.com/devops-tools-report-2020/)*

---

![h:600 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/PeriodicTableDevOpsTools.png)

*[https://digital.ai/periodic-table-of-devops-tools](https://digital.ai/periodic-table-of-devops-tools)*

---
# Some Tools for Each Phase
## *(This list is per nature not Exhaustive and not Up-to-Date)*

---

**Plan** : Jira, Confluence, Trello

**Code** : 
- Version Control Systems :  Git
- IDE : VSCode, IntelliJ
- Developper portals : Backstage, Port

**Build** : Maven, Graddle

**Test** : Selenium, JUnit, Mocha, Postman, JMeter, Gatling

**Release / Integration** : Jenkins, CircleCI, GitLab CI/CD

---
## Deploy 
- Application deployment : 
  - Docker
  - Kubernetes (Ranche, OpenShift, ..)
  - Docker Swarm
  - Nomad
- Infrastructure as code (IaC)
   - Terraform
   - CloudFormation
   - Pulumi
   - Ansible 
   - Puppet
   - Chef

---
## Operate / Monitor

- Prometheus
- Grafana
- ELK Stack (Elasticsearch, Logstash, Kibana)
- Splunk
- Datadog
- New Relic
- Zabbix

---
## Security (DevSecOps)
   - Snyk
   - Aqua Security
   - HashiCorp Vault

## Collaboration and communication (ChatOps)
   - Slack
   - Microsoft Teams
   - Mattermost
   - Discord


---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)
---

# Conclusion

---
# Conclusion

- A set of best practices
- Based on empiricism, the acquisition of knowledge through experience
- Five axes: **C**ulture, **A**utomation, **L**ean, **M**easurement, **S**haring
- The tooling can be seen as a pipeline and covers the entire workflow
- To adopt DevOps, you need: a vision, skills, motivation, resources, and a plan.

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Sources / Références

---

- [https://openclassrooms.com/fr/courses/6093671-decouvrez-la-methodologie-devops](https://openclassrooms.com/fr/courses/6093671-decouvrez-la-methodologie-devops)
- [https://medium.com/publicis-sapient-france](https://medium.com/publicis-sapient-france)
- [https://medium.com/edureka/devops-lifecycle-8412a213a654](https://medium.com/edureka/devops-lifecycle-8412a213a654)
- [https://red-green-refactor.com/2020/11/04/book-club-the-devops-handbook-chapter-1-agile-continuous-delivery-and-the-three-ways/](https://red-green-refactor.com/2020/11/04/book-club-the-devops-handbook-chapter-1-agile-continuous-delivery-and-the-three-ways/)
