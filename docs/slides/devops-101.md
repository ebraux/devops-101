---
marp: true
paginate: true
theme: default
---
<!--  #0074d0 
 le marron-->
<!-- #ff5001 le orange -->
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:40% 100%](assets/devops.png)



# Devops 101

## Introduction au Devps
###   Principes de base

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](assets/cc-by-nc-sa.png)

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- ## > Définition
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


---

# Définition

"Le DevOps est un **mouvement** en ingénierie informatique et une **pratique** technique visant à l'unification du développement logiciel (**DEV**) et de l'administration des infrastructures informatiques (**OPS**), notamment l'administration système"
(Wikipedia)"
  
*[https://fr.wikipedia.org/wiki/Devops](https://fr.wikipedia.org/wiki/Devops)*


---
### Une méthodologie qui gère le développement et la livraison de logiciels :
  - Développement continu
  - Appliquer rapidement les changements en production (problèmes, exigences)
  - Automatisation pour améliorer l'efficacité

### DevOps combine en un seul Workflow de travail intégré :
  - Personnes, processus et technologies
  - Développement, tests et opérations de logiciels

### Utilise des principes agiles, des meilleures pratiques et des outils pour un développement logiciel optimal

---
# Cycle de vie DevOps

![bg left:40% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/ci-cd.png)

- Itération en sprints
- Commence par la **planification**
- Ensuite, les exigences sont converties en **code**, **construites** et **testées**.
- Puis la **mise en production** est générée automatiquement
- Suivi par le **déploiement** en production.
- Le logiciel est **exploité** et **surveillé**.
- Les Feed back sont utilisés comme nouvelles exigences
- Le cycle se répète

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- ## >Contexte
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


--- 
# Les DEVs

- Toute personne impliquée dans **la fabrication du logiciel avant qu’il n’atteigne la production**
- Les développeurs, les gestionnaires de produits, les testeurs, les Product Owners ...
- Chargés de **produire de l’innovation** et délivrer les **nouvelles fonctionnalités** aux utilisateurs dès que possible
- Recherchent des **changements**

--- 
# Les OPS

- Toute personne impliquée dans **l’exploitation et la maintenance de la production**
- Les ingénieurs systèmes, les DBAs, les ingénieurs réseaux,  ...
- Chargés de s’assurer que les utilisateurs ont accès à un **système stable, rapide et réactif** 
- Recherchent la **stabilité** organisationnelle


---
## Processus de déploiement logiciel : côté DEV

- Définition des fonctionnalités du produit
- Définition des prérequis
- Planification 
- Développement
- Construction des packages
- Livraison d'une version de l'application pour le déploiement en production

---
## Processus de déploiement logiciel : côté OPS

- Déploiement des infrastructures
- Installation des pre-requis
- Déploiement de l'application
- Suivi de l'application (incidents, ...)
- Suivi des performances

---
# Organisation : Modèle en cascade (WaterFall)

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cascade_avec_livrables.png)

---
![bg left:30% % opacity:.6](img/cascade_avec_livrables.png)
# Organisation : Modèle en cascade (WaterFall)

- Phases linéaires 
- Spécialisation des tâches
- Chaque phase dépend des résultats de la phase précédente
- En cas de défaut : retour à la phase précédente
- Impact très fort des exigences

*[https://fr.wikipedia.org/wiki/Mod%C3%A8le_en_cascade](https://fr.wikipedia.org/wiki/Mod%C3%A8le_en_cascade)*

---
# Organisation : Cycle en V

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/cycle_en_v.png)


---
![bg left:30% % opacity:.6](img/cycle_en_v.png)
# Organisation: Cycle en V
- Modèle cascade, mais en renforçant la validation
- Flux descendant,
  - détailler le produit jusqu'à sa réalisation
- Flux ascendant,
   -  valider le produit jusqu'à sa "recette" au client
- Découpage système / composants
- Impact toujours très fort des exigences
- "temps long" : évolution demande client ?

*[https://fr.wikipedia.org/wiki/Cycle_en_V](![](https://fr.wikipedia.org/wiki/Cycle_en_V))*

---
# Organisation : Méthode Agile - Scrum

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/vueglobalescrum.png)

---
![bg left:30% % opacity:.6](img/vueglobalescrum.png)
# Organisation Méthode Agile - Scrum

- Planification adaptative, des itérations
- Développement évolutif,
- Livraison précoce
- Amélioration continue,
- Flexible au changement
- La méthode Scrum implémente l'approche agile

*[https://fr.wikipedia.org/wiki/M%C3%A9thode_agile](https://fr.wikipedia.org/wiki/M%C3%A9thode_agile)*
*[https://fr.wikipedia.org/wiki/Scrum_(d%C3%A9veloppement)](https://fr.wikipedia.org/wiki/Scrum_(d%C3%A9veloppement))*

---
![bg left:25% 90% ](img/ITIL-Cycle-de-vie-des-services.png)
# Organisation : ITIL

**I**nformation **T**echnology **I**nfrastructure **L**ibrary

Ensemble de bonnes pratiques pour la gestion des SI

Un référentiel méthodologique : 

- Comment organiser un système d'information ?
- Comment améliorer l'efficacité du système d'information ?
- Comment réduire les risques ?
- Comment augmenter la qualité des services informatiques ?

*[https://fr.wikipedia.org/wiki/Information_Technology_Infrastructure_Library](https://fr.wikipedia.org/wiki/Information_Technology_Infrastructure_Library)*

---
![bg left:50% 90% ](img/ITIL.jpg)

# Organisation : ITIL

- Catalogue de services,
- Planification,
- Stratégie,
- Gouvernance,
- Amélioration continue
- Gestion des risques,
- ...

---
# DEV et OPS : des visions différentes

| DEV | OPS |
|:------------:|:-----------:|
| Planning et la date de livraison | Qualité de service et disponibilité |
| Dernières technologies | Technologies standards |
| Changements Fréquents et importants | Minimiser les changements - Incidents  |
| Méthodes Agiles | Cycle en V - ITIL |
| Attendent les OPS | Subissent des DEVS |

--- 

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/WallOfConfusion-1-768x341.png)

---

![bg left:60% 60% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/Code-bombing.png)
# Et Bien souvent

## Hop ...
  

---

# DEV et OPS : des motivations différentes

Critères pris en compte pour les primes, promotions, ...

- DEV : 
  - nombre de bugs corrigés
  - nombre de nouvelles fonctionnalités livrées 
- OPS :
  - disponibilité globale sur l'année de l'application.

---
# DEV et OPS les seuls concernés ?

- importance du **"Time to Market"** : temps entre la décision de création d'une fonctionnalité, et sa disponibilité sur le produit final en production.
- assurer une bonne expérience client : fiabilité, stabilité, performance
- le coût
- ...
  
Marketing, finances, ressources humaines, ...


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- ## > Origine et concepts
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


--- 
![bg left:25% %](img/patrick_debois.png)
# Comment s'y prendre

Question que c'est posé Patrick Debois, en 2007 ...
- plusieurs postes dans des grandes entreprise depuis 15 ans : développeur, ingénieur réseaux, administrateur système et chef de projet
- Bonne expérience technique et organisationnelle
- **Testeur** sur un projet de migration de datacenter
- Doit collaborer des DEV et des OPS, et concilier les métiers, travailler de facçon efficace
- Et c'est compliqué ....
  
*[https://www.linkedin.com/in/patrickdebois/](https://www.linkedin.com/in/patrickdebois/)*
*[https://www.jedi.be/](https://www.jedi.be/)*

---
# Trouver des solutions ...

- 2008 : présentation "agile infrastructure", par Adrew Shafer (RedHat)
  - Faire travailler les OPS de façon agile.
  - Peu de participants (dont Patrick Debois)
  - Création d'un groupe Google : faible engouement.
- 2009 : Velocity O'Reilly, Dev and Ops cooperation at Flickr,  John Allspaw et Paul Hammond 
  - collaboration, expérience positive, ...

*[https://groups.google.com/g/agile-system-administration](https://groups.google.com/g/agile-system-administration)*
*[https://www.slideshare.net/jallspaw/10-deploys-per-day-dev-and-ops-cooperation-at-flickr](https://www.slideshare.net/jallspaw/10-deploys-per-day-dev-and-ops-cooperation-at-flickr)*
*[https://www.youtube.com/watch?v=LdOe18KhtT4](https://www.youtube.com/watch?v=LdOe18KhtT4)*


---
# Naissance du Devops

- Octobre 2009 :  les "Devopsdays"
  - Patrick Debois n'a pas pu assister à la présentation "Dev ans Ops cooperation at Flickr"
  - Il crée alors les "DevopsDay", à Gand en Belgique
  - Très gros succès : dev, ops, managers, responsables


- 2010 : Naissance du hashtag Twitter **#DevOps**
  
*[https://devopsdays.org/](https://devopsdays.org/)*

---
# Tendance depuis 2009

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-historique.png)

*[https://trends.google.fr/](https://trends.google.fr/)*

---
![bg 80% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-monde.png)
![bg 20% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/trends-france.png)


---
![bg left:25% 95%](img/DevOpsDays.png) 
# Objectifs du Devops

- Satisfaire l’utilisateur final de la solution développée. 
- Accélérer les déploiements applicatifs
- Améliorer la qualité
- Réduire le "Time-to-Market"

---
![bg left:25% 95%](img/DevOpsDays.png) 
#  Principes du Devops

- Un ensemble de bonnes pratiques
- Basé sur l'empirisme, l'acquisition de connaissances par l'expérience
- Faire participer toutes les équipes (pas uniquement les Dev et les Ops) à l’intégralité du cycle de vie de services
- Créer une culture commune

---
![bg left:25% 95%](img/DevOpsDays.png) 
# Améliorer l’efficacité d'un projet informatique

- En proposant des **déploiements en continu**,
- Tout en conservant la **stabilité des environnements**
- Grâce à du **monitoring** ou des **sécurités applicatives**.
- Et utilisation de l'**automatisation** (test, déploiement)

---
![bg left:25% 95%](img/DevOpsDays.png) 
# Le DevOps

- Créé par des acteurs de l'IT pour des utilisateur de l'IT
- N'appartient a aucune organisation, éditeurs, ni analystes, ...
- pas un produit, pas une spécification, pas un standard
- pas un titre : un ingénieur devops ça n'existe pas


---  
![bg left:45% 100%](img/calms-model-devops.png) 
# Les piliers du Devops. 

**Culture**: se sentir pleinement impliqué, collaboration et communication
**Automatisation**: Automatisation des tâches répétitives, Déploiement continu et Infrastructure as Code.
**Lean**: production de valeur, minimisation des coûts et des "déchets" - optimisation des processus
**Mesure**: collecter des données en continu,  pour s'améliorer.
**Partage**: connaissances, outils et meilleures pratiques

---  
![bg left:45% 95%](img/DevOps-schema.png) 
# Les Phases du DevOps

- **Plan** : Planification
- **Code** : Réalisation
- **Build** : Assemblage
- **Test**: Test
- **Release** : Versionnage
- **Deploy** : Déploiement
- **Operate** : Opérationnalisation
- **Monitor** : Surveillance

---  
![bg left:40% 90%](img/devops-methods.png) 
# Les Méthodes du devops

- "CI/CD" : Continuous integration, Continuous delivery/deployment
- "Infrastructure As Code"
- "Shift left Testing"
- ...  

---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- ## > Les Piliers du devops : CALMS
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Les piliers du Devops. 

- **Culture**: Les équipes doivent se sentir pleinement impliquées dans les projets - collaboration, communication, conduite du changement
- **Automatisation**: Automatiser les tâches répétitives pour augmenter l'efficacité et réduire les erreurs humaines - Déploiement continu et Infrastructure as Code.
- **Lean**: Optimiser les processus, éliminer les gaspillages, réduire les coûts - stratégie orientée "valeur ajoutée"
- **Mesure**: collecter continuellement des données pour s'améliorer
- **Partage**: connaissances, outils et meilleures pratiques - améliorer la collaboration et l'innovation.

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Culture 

**Le DevOps est avant tout une histoire de culture et de collaboration.**

- Le devops exige le changement de la culture organisationnelle
- Il faut créer la volonté de travailler ensemble
-   et apprendre à le faire ...
- Création d'équipes orientées "produit"
- Accepter l'Erreur

**Inspiré de l'agilité : "Les individus et les interactions plutôt que les processus et les outils"**

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Automatisation

**"Le déploiement d'une nouvelle version ne doit plus être un événement"**

- Déployer plus souvent
- Mieux contrôler, tester/répéter les déploiements
- Outils d'Infrastructure-as-Code : environnements  versionables et duplicables
- Le déploiement de l'application et de son environnement d'execution doit être reproductible
  
**Le déploiement = un non événement**


---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Lean

**Lean, ou Lean Management est une méthode de gestion de production, centrée sur la rationalisation des opérations**

**Délivrer de la valeur ajoutée au client final, en minimisant  les processus, longs, coûteux, sans valeur ajoutée.**

- Réduire la taille des déploiements
- Mettre en place une chaîne de valeur "Value Stream map" (VSM)
- En cas de problème ou d'incident faire un bilan, une rétrospective
- Amélioration continue
- ...


---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS -  Measurement (Mesures)

Le Devops transforme l'entreprise, mais demande des efforts

- Quel est son impact ?
- Est-il positif ou négatif ?
- Quelle est la dynamique ?
   
**Il est nécessaire d'avoir des indicateurs de performance clés (KPI ou Key Performance Indicator)**

- Nombre d'apparition d'un bug récurrent
- Nombre d'utilisateurs gagnés ou perdus en une semaine
- ...
 

---
![bg left:20% 60%](img/keep-CALMS.png) 
# CALMS - Sharing

- Faire circuler l'information
- Impliquer, partager les responsabilités
- Organiser le transfert de compétence, la formation
- Produire des documents compréhensibles par tous
- Favoriser la collaboration au sein du groupe produit


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- ## > Les Phases du DevOps
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


---

![bg left:40% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4](img/DevOps-schema.png ) 
# Les phases du DevOps

- Plan : Planification
- Code : Réalisation
- Build : Assemblage
- Test: Test
- Release : Versionnage
- Deploy : Déploiement
- Operate : Opérationnalisation
- Monitor : Surveillance

*[https://fr.wikipedia.org/wiki/Cha%C3%AEne_d%27outils_Devops](https://fr.wikipedia.org/wiki/Cha%C3%AEne_d%27outils_Devops)*

---
![bg left:25% % opacity:.7](img/plan.png)
# Plan : Planification

**Imaginer, et décrire les fonctionnalités des applications et des systèmes**

Et définir :
- Les exigences
- La valeur commerciale
- Les Indicateurs, Métriques, et retours d’expérience de production
- Le timing et analyse de rentabilisation des releases
- La politique de sécurité.
- ...
---
![bg left:25% % opacity:.7](img/code.png)
#  Code : Réalisation

**Ecriture du code, et configuration du processus de développement de logiciel.**

Tous les aspects du codage 
- Écriture,
- Tests, contrôles de qualité et de performance
- Révisions
  
---
![bg left:25% % opacity:.7](img/build.png)
# Build : Assemblage

**Assemblage des différents éléments de code pour "construire" l'application**

- Concerne le code produit par les différents développeurs
- Intégration du code par les membres de l'équipe,
- Préparation de la livraison.

---
![bg left:25% % opacity:.7](img/test.png)
# Test: Test

**Test non pas au niveau du code, mais au niveau de l'application**

- Test d’acceptation,
- Test de régression,
- Analyse de sécurité et vulnérabilité,
- Test de performance, de charge,
- Test de résilience

---
![bg left:25% % opacity:.7](img/release.png)
# Release : Versionnage

**Livraison d'une version de l'application**

- Suivi, Coordination des livraisons
- Planification, traçabilité
- Archivage des éléments nécessaires (Artifacts)

Conservation de l'historique en cas de nécessité de retour en arrière

---
![bg left:25% % opacity:.7](img/deploy.png)
# Deploy : Déploiement

**Déployer des applications dans des environnements de production de manière cohérente et fiable**

- Intègre le déploiement et la configuration de l'infrastructure de base
- Possibilité d'approbations manuelles sur certaines parties


---
![bg left:25% % opacity:.7](img/operate.png)
# Operate : Opérationnalisation

**Suivi de l'application dans l'infrastructure**

  Monitoring, Orchestration
- Absorber les pics de charge
- Ajustement de l'architecture de l'infrastructure
  - Modification du nombre de ressources (scale up / down)
  - Découpage, mise en place de Load-Balancer, ...

---
![bg left:25% % opacity:.7](img/monitor.png)
# Monitor : Surveillance

Implique la maintenance, la supervision et le dépannage des applications dans les environnements de production.

- Performances de l'infrastructure
- L’expérience et la réaction de l’utilisateur final
- Les métriques et les statistiques de production.
- En complément de "operate", mais avec un objectif de retour vers les autres phases pour amélioration.

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- ## > Les Méthodes du devops
- **Implémentation défis et risques**
- **Les Métiers**
- **Les Outils Techniques**


---
# CI/CD -  Continuous integration 

**Mettre à disposition du code récent pour tous les développeurs**

- Permettre des cycles courts,
- Construire, tester et diffuser un logiciel plus rapidement.

Phases :
- "Commit" du code dans le dépôt, et notification
- Lancement de l'intégration du code, pour le vérifier
  - Test de build du code : qualité de code, ...
  - Test du code : intégration et acceptance

---
# CI/CD - Qualité du code

**Objectif : unifier le code entre tous les développeurs**

Définir le standard de code qui va être utilisé.
  -  Manière de coder, règles à respecter, ... : 

Respecter les standards communs :
  - Exemple de standard : PEP8, FLAKE8, Pylint, ...

---
# CI /CD - Continuous Delivery / Deployment

**Déployer les modifications de façon systématique et contrôlée**

- Livraison fréquentes et automatisées
- Réduit les coûts, le temps et les risques inhérents aux changements.
- Toujours une version disponible pour être utilisé par les clients (releasable)
- Possibilité de revenir en arrière si besoin

- _**Delivery**_ : Le déploiement reste manuel.
- _**Deployment**_ : L'application est déployée automatiquement.

---
# Gestion de version

**Collaboration, répartition des taches, developpement en parallèle, historisation**

- Suivre les révisions et l'historique des modifications
- Faciliter l'examen du code, le retour à une version antérieure
- Fusionner les modifications de code, gérer les conflits
- Systèmes de gestion de versions le plus répendu : Git

*[https://fr.wikipedia.org/wiki/Git](https://fr.wikipedia.org/wiki/Git)*

---
# Infrastructure as Code

**Concevoir des infrastructure et système de façon descriptive**

- Des fichier au format texte (généralement yaml)
- Des outils de déploiement et suivi
- Gestion comme du code : suivi en version, tests, ...
- Déploiements fiables, reproductibles et contrôlés.
- Moins de risques d'erreur humaine
- Duplication des environnements
- Systèmes d'infrastructure as code les plus rependus : Ansible, Terraform.

---
# Le "Shift Left Testing"

**Plus un bug, ou un défaut est détecté tard dans la chaîne de production, plus son impact est important**

Intégrer des tests automatiques le plus tôt possible. 


![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/Shift-Left-Testing.png)

---
![bg left:30% 80%](img/3-ways.png)
# Three ways

De Gene Kim, Livres  "devops Handbook the phoenix project"

- "First Way" : notion de **flux**
- "Second Way" : importance du **feedback**
- "Third Way" : expérimenter et **continuer** d'apprendre

--- 
## "First Way" : notion de flux
- Importance de la performance de tout le système, et non par ses individualités.
- Interdire les optimisations locales
- Identifier le facteur limitant le plus important (la contrainte)

## "Second Way" : importance du feedback
- Réduire les délais des boucles de feedback 
- retours des clients soient pris en compte le plus rapidement possible 
- Peer review : faire relire le code

---
## "Third Way" : expérimenter et continuer d'apprendre
- Créer une culture d'expérimentation continue"
- Allouer du temps pour améliorer le travail quotidien
- Créer des rituels, 
- Planifier du temps pour découvrir (Test, Hackathon, ...)

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- ## > Implémentation défis et risques
- **Les Métiers**
- **Les Outils Techniques**

---
# Culture

**Gérer le Changement Efficacement**
- Mettre en œuvre des pratiques de gestion du changement pour gérer les changements en douceur et minimiser les perturbations.

> Résistance Culturelle : La résistance au changement au sein de l'organisation peut entraver, voir empêcher l'adoption des pratiques DevOps.

---
# Collaboration

**Favoriser une Culture Collaborative**:
- Encourager la communication ouverte et la coopération entre le développement, les opérations et les autres parties prenantes.

> Mauvaise Collaboration : Le manque de communication et de coopération peut saper les efforts DevOps.

---
# Compétences

**Encourager l'Apprentissage Continu et l'Amélioration**
- Promouvoir une culture d'apprentissage continu et d'amélioration grâce à des formations régulières, des retours d'expérience et des expérimentations.

> Manque de Personnel Qualifié : Une expertise insuffisante dans les outils et pratiques DevOps peut conduire à une mise en œuvre inefficace.

---
# Automatisation

**Automatiser Tout**
- Automatiser les tâches répétitives telles que les tests, le déploiement et la provision d'infrastructure pour augmenter l'efficacité et réduire les erreurs.

**Adopter l'Infrastructure en tant que Code (IaC)** :
- Gérer et provisionner l'infrastructure comme du code pour assurer la cohérence et la reproductibilité.

> Automatisation Inadéquate : Le manque d'automatisation des processus clés peut entraîner des inefficacités et des erreurs.

---
# Intégration Continue/Déploiement Continu (CI/CD)

**Mettre en Œuvre l'Intégration Continue/Déploiement Continu (CI/CD)** :
- Intégrer les modifications de code fréquemment et les déployer automatiquement dans les environnements de production.

> Sprint trop ambitieux : Les modifications trop importantes/complexes  on un impact négatif sur la fréquence de déploiement

> Attentes Irréalistes : Attendre des résultats immédiats sans comprendre que DevOps est un processus d'amélioration continue peut conduire à des déceptions.

---
# Surveillance

**Surveillance et Journalisation Continues** :
- Surveiller en continu les applications et l'infrastructure pour détecter et résoudre les problèmes rapidement.

**Mesurer et Analyser les Performances** :
- Utiliser des métriques et des analyses pour mesurer les performances et identifier les domaines à améliorer.

> Surveillance et Journalisation Insuffisantes : Sans une surveillance et une journalisation appropriées, il est difficile de maintenir la santé et les performances des applications.

---
# Sécurité

**Intégrer les Pratiques de Sécurité (DevSecOps)** :
- Intégrer les pratiques de sécurité tout au long du cycle de développement pour assurer une livraison sécurisée des logiciels.

> Impact de la sécurité : des pratiques de sécurité trop complexes ou trop contraignantes sont souvent contournées
 
> Négligence de la Sécurité : Négliger les pratiques de sécurité peut entraîner des vulnérabilités

---
# Outils

**Utiliser les Bons Outils** :
- Sélectionner et intégrer des outils adaptés aux workflow, simples et efficaces.
  
> Surcharge d'Outils : Utiliser trop d'outils sans une intégration appropriée peut créer de la complexité et de la confusion.


---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- ## > Les Métiers
- **Les Outils Techniques**


---
# SRE  (Site Reliability Engineering)

**Objectif : créer de système évolutifs et fiables**

- Apparu chez Google.
- Donner des taches opérationnelles à des ingénieurs logiciel.

Ingénieur SRE :
- 50% : taches opérationnelles (incidents, interventions manuelles, ...)
- 50% : taches de développement (nouvelles fonctionnalités, automatisation de taches, ...)


---
# DevSecOps

**Créer un état d'esprit global, dans lequel tout le monde se sente responsable de la sécurité del'organisation** 

- Les dev et ops
- Les manager
- Le personnel administratif (secrétaires, ...)

**En trouvant un juste milieu pour être réactif, et ne pas gêner le travail.**

- Security as code, security by design
- S'intégrer au "shiftleft"

---
# ChatOps

**Objectif : Optimiser les flux de communication**

- Connecter les personnes, les outils, les fichiers, ...
- Diffuser des messages automatiques :
  - Quand un développement, ou un déploiement est effectué,
  - Lorsqu'un support est nécessaire
- Réduire le MTTR (Mean Time To repair)
  - Boucle de feedback plus courte
  - Interaction directe entre les clients et les développeurs

Utilisation de BOT dans des outils type Slack, Teams, Mattermost, ...


---

<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Introduction Au Devops

- **Définition**
- **Contexte**
- **Origine et concepts**
- **Les Piliers du devops : CALMS**
- **Les Phases du DevOps**
- **Les Méthodes du devops**
- **Implémentation défis et risques**
- **Les Métiers**
- ## > Les Outils Techniques


---

# Les outils Techniques

- Pas d'outils "DevOps",
- Des outil "Supportant la démarche DevOps".
- Attention aux outils d’automatisation ou de déploiement « rebrandés »
- Énormément d'outils
- Certains déjà utilisés dans l'entreprise

---
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/gittoolsReport2020.png)

*[https://extasius.com/devops-tools-report-2020/](https://extasius.com/devops-tools-report-2020/)*

---

![h:600 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/PeriodicTableDevOpsTools.png)

*[https://digital.ai/periodic-table-of-devops-tools](https://digital.ai/periodic-table-of-devops-tools)*

---
# Quelques Outils pour Chaque Phase
## *(Cette liste n'est par nature pas exhaustive et pas à jour)*

---

**Plan** : Jira, Confluence, Trello

**Code** : 
- System de controle de version :  Git
- IDE : VSCode, IntelliJ
- Portail Developpeur : Backstage, Port

**Build** : Maven, Graddle

**Test** : Selenium, JUnit, Mocha, Postman, JMeter, Gatling

**Release / Integration** : Jenkins, CircleCI, GitLab CI/CD

---
## Deploy 
- Déploiement d'application : 
  - Docker
  - Kubernetes (Ranche, OpenShift, ..)
  - Docker Swarm
  - Nomad
- Infrastructure as code (IaC)
   - Terraform
   - CloudFormation
   - Pulumi
   - Ansible 
   - Puppet
   - Chef

---
## Operate / Monitor

- Prometheus
- Grafana
- ELK Stack (Elasticsearch, Logstash, Kibana)
- Splunk
- Datadog
- New Relic
- Zabbix

---
## Securité (DevSecOps)
   - Snyk
   - Aqua Security
   - HashiCorp Vault

## Collaboration et communication (ChatOps)
   - Slack
   - Microsoft Teams
   - Mattermost
   - Discord




---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)
---

# Conclusion

---

# Conclusion

- Un ensemble de bonnes pratiques
- Basé sur l'empirisme, l'acquisition de connaissances par l'expérience
- Cinq axes: Culture, Automatisation, Lean, Mesure, Partage
- L’outillage peut se voir comme un pipeline et couvre tout le workflow
- Pour adopter Devops il faut : une vision, des compétences, une motivation, des ressources et un plan.

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80%](assets/devops.png)

# Sources / Références

---

- [https://openclassrooms.com/fr/courses/6093671-decouvrez-la-methodologie-devops](https://openclassrooms.com/fr/courses/6093671-decouvrez-la-methodologie-devops)
- [https://medium.com/edureka/devops-lifecycle-8412a213a654](https://medium.com/edureka/devops-lifecycle-8412a213a654)
(https://streaming-canal-u.fmsh.fr/vod/media/canalu/documents/jdev/jdev2020.t6.keynote.devops.presentation.generale.et.bien.plus._56703/devops4null.2020.pdf)
- [https://red-green-refactor.com/2020/11/04/book-club-the-devops-handbook-chapter-1-agile-continuous-delivery-and-the-three-ways/](https://red-green-refactor.com/2020/11/04/book-club-the-devops-handbook-chapter-1-agile-continuous-delivery-and-the-three-ways/)
