# Introduction au Devops / Introduction to DevOps

---

## Devops 101 :

Slides de la présentation:

* Version en ligne: [devops-101.html](slides/devops-101.html)
* version PDF: [devops-101.pdf](slides/devops-101.pdf)


Presentation slides:

* Online version: [devops-101-en.html](slides/devops-101-en.html)
* PDF version: [devops-101-en.pdf](slides/devops-101-en.pdf)


---

## Les Piliers du Devops - CALMS :

Slides de la présentation:

* Version en ligne: [devops-calms.html](slides/devops-calms.html)
* version PDF: [devops-calms.pdf](slides/devops-calms.pdf)

---

![image alt <>](assets/devops.png)