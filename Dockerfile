FROM python:3.8-buster

LABEL maintainer="Emmnanuel Braux <emmanuel.braux@imt-atlantique.fr>"

USER root

COPY requirements.txt  /srv/requirements.txt

RUN pip install -r /srv/requirements.txt

WORKDIR /work

